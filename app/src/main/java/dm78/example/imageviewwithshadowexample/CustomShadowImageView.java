package dm78.example.imageviewwithshadowexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class CustomShadowImageView extends ImageView {

    public static final String TAG = CustomShadowImageView.class.getSimpleName();
    public static final float SHADOW_RADIUS_DP = 3f;
    public static final float SHADOW_X_OFFSET_DP = 2f;
    public static final float SHADOW_Y_OFFSET_DP = 2f;

    private Paint mPaint;
    private float mShadowRadius;
    private float radius;
    private float cx;
    private float cy;
    private float mShadowXOffset;
    private float mShadowYOffset;
    private Bitmap mShadowBitmap;
    private FrameLayout.LayoutParams layoutParams;
    private boolean expanded;

    public CustomShadowImageView(Context context) {
        super(context);
        init();
    }

    public CustomShadowImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomShadowImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Log.d(TAG, "init " + this.hashCode());

        DisplayMetrics dm = getContext().getResources().getDisplayMetrics();
        mShadowRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SHADOW_RADIUS_DP, dm);
        mShadowXOffset = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SHADOW_X_OFFSET_DP, dm);
        mShadowYOffset = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SHADOW_Y_OFFSET_DP, dm);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //noinspection deprecation
        int shadowColor = getContext().getResources().getColor(R.color.shadow);
        mPaint.setColor(shadowColor);

        expanded = false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG, String.format("onMeasure %d w: %d, h: %d", this.hashCode(), MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec)));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d(TAG, String.format("onLayout %d changed: %b, l: %d, t: %d, r: %d, b: %d", this.hashCode(), changed, left, top, right, bottom));
        super.onLayout(changed, left, top, right, bottom);

        if (changed) {
            if (!expanded) {
                layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
                layoutParams.width = (int) (layoutParams.width + mShadowXOffset);
                layoutParams.height = (int) (layoutParams.height + mShadowYOffset);
                expanded = true;
            }

            cx = (right - left) / 2 + mShadowXOffset;
            cy = (bottom - top) / 2 + mShadowYOffset;

            boolean widthGreater = (right - left) > (bottom - top);
            radius = (widthGreater ? right - left : bottom - top) / 2;

            if (mShadowBitmap == null) {
                Bitmap bitmap = Bitmap.createBitmap(right - left, bottom - top, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                canvas.drawCircle(cx, cy, radius, mPaint);

                if (Build.VERSION.SDK_INT >= 17 && !isInEditMode()) {
                    RenderScript rs = RenderScript.create(getContext());
                    Allocation input = Allocation.createFromBitmap(rs, bitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
                    Allocation output = Allocation.createTyped(rs, input.getType());
                    ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                    script.setRadius(mShadowRadius);
                    script.setInput(input);
                    script.forEach(output);
                    output.copyTo(bitmap);
                }

                mShadowBitmap = bitmap;
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw " + this.hashCode());
        canvas.drawBitmap(mShadowBitmap, mShadowXOffset, mShadowYOffset, null);
        super.onDraw(canvas);
    }
}
