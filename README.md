I'm attempting to extend ImageView and add a shadow.  I'm running into a 
problem where the shadow is being clipped by the view bounds and looks quite 
bad.  

I've attempted to set the width/height via LayoutParameters 
programmatically as well as trying different XML properties like 
android:adjustViewBounds, but there is no change in the display.  Similarly,
setting a android:layout_margin is ineffective in preventing the shadow from 
being clipped.